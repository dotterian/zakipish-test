import * as React from 'react'
import SortableTree from 'react-sortable-tree'
import * as data from './json.json'
import 'react-sortable-tree/style.css';

type Entity = [number, string, number]
type Data = {
  [key: number]: Entity
}
type EntityObject = {
  id: number,
  title: string,
  children: EntityObject[]
}

type State = {
  data: EntityObject[]
}

export default class Application extends React.Component<{}, State> {

  constructor(props: {}) {
    super(props)
    this.state = {
      data: this.gatherChildren(0, data as Data)
    }
  }

  gatherChildren(parentId: number, array: Data): EntityObject[] {
    const children: EntityObject[] = []

    for (let i in array) {
      if (array[i][2] !== parentId) {
        continue
      }
      const newEntity: EntityObject = {
        id: array[i][0],
        title: array[i][1],
        children: this.gatherChildren(array[i][0], array)
      }
      children.push(newEntity)
    }

    return children;
  }

  flattenData(array: EntityObject[], parentId: number): Entity[] {
    let newArray: Entity[] = []
    for (let entity of array) {
      newArray.push([entity.id, entity.title, parentId])
      if (entity.children.length > 0) {
        const children = this.flattenData(entity.children, entity.id)
        newArray = newArray.concat(children)
      }
    }
    return newArray
  }

  downloadJSON = () => {
    const data = this.flattenData(this.state.data, 0)
    const a = document.createElement('a')
    const file = new Blob([JSON.stringify(data)], {type: 'application/json'})
    a.href = URL.createObjectURL(file);
    a.download = "newJson.json";
    a.click();
  }

  render () {
    return <div style={{height: '100vh'}}>
      <SortableTree
        treeData={this.state.data}
        onChange={(data: EntityObject[]) => this.setState({
          data
        })}
      />
      <button
        style={{position: 'fixed', top: "20px", right: "50px", fontSize: '16px', padding: '5px 10px'}}
        onClick={this.downloadJSON}
      >
        Получить итоговый JSON
      </button>
    </div>
  }
}
